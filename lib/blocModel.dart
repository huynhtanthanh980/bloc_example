import 'dart:async';

import 'package:rxdart/rxdart.dart';

class Increment {

  int counter = 0;

  Sink get addition => _additionController.sink;
  final _additionController = StreamController();

  Stream<String> get pressCount => _pressCountSubject.stream;
  final _pressCountSubject = BehaviorSubject<String>();

  Increment() {
    _additionController.stream.listen(_increase);
  }

  void _increase(Object itDoesNotMatter) {
    _pressCountSubject.add((++counter).toString());
  }

  dispose(){
    _additionController.close();
    _pressCountSubject.close();
  }
}